# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://enjan@bitbucket.org/enjan/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/enjan/stroboskop/commits/da2353584d9a6350f8d6031b7695adc38c2b2c37?at=master

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/enjan/stroboskop/commits/8a981d9f71a4f3468bea660abff67cb82b17c471?at=master

Naloga 6.3.2:
https://bitbucket.org/enjan/stroboskop/commits/8c657916d77aa17043ed0015621c1ca7d44871de?at=master

Naloga 6.3.3:
https://bitbucket.org/enjan/stroboskop/commits/53ef9c1db40650a19c2d19947f6239b877ea7dfd?at=master

Naloga 6.3.4:
https://bitbucket.org/enjan/stroboskop/commits/c5e688c5921201ad20ed3f9ff5cf19f79c092411?at=master

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/enjan/stroboskop/commits/b8da3b09c792caa3ec5eaa8a081c1a51dee1eae6?at=master

Naloga 6.4.2:
https://bitbucket.org/enjan/stroboskop/commits/bb3d493eb2a132d1375f9f30f66d3cbd4bc12b86?at=master

Naloga 6.4.3:
https://bitbucket.org/enjan/stroboskop/commits/4a4bf4ccd50a02cf2eb266fe438ce72645453305?at=master

Naloga 6.4.4:
https://bitbucket.org/enjan/stroboskop/commits/f5f9edf1196bccc1f732e864d021ce2f3da8e29d?at=master
